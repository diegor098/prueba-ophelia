import { Injectable } from '@angular/core';
import {
  HttpClient, HttpErrorResponse
} from "@angular/common/http";
import { Observable } from 'rxjs';
import { Productos } from 'src/models/Productos';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor(public http: HttpClient) { }

  public getProductos() {
    const vUrl = 'http://localhost:64741/api/productos';
    return this.http
      .get(vUrl)
  }

  public saveProductos(producto : Productos) {
    const vUrl = 'http://localhost:64741/api/productos';
    return this.http
      .post(vUrl,producto)
  }

  public updateProductos(producto : Productos) {
    const vUrl = 'http://localhost:64741/api/productos/'+producto.id_producto;
    return this.http
      .put(vUrl,producto)
  }

  public deleteProductos(producto : Productos) {
    const vUrl = 'http://localhost:64741/api/productos/'+producto.id_producto;
    return this.http
      .delete(vUrl)
  }

}
