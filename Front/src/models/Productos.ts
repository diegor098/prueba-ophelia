export class Productos{

    public id_producto:number;
    public nombre:string;
    public unidades:number;
    public valor: number;


    constructor(){
        this.id_producto = 0;
        this.nombre = "";
        this.unidades = 0;
        this.valor = 0;
    }
}