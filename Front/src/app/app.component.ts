import { Component, OnInit } from '@angular/core';
import { Productos } from 'src/models/Productos';
import { AppService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  productos: Productos[] = [];

  constructor(private service: AppService) {

  }

  ngOnInit() {
    this.getProductos();
  }

  getProductos() {
    this.service.getProductos().subscribe(
      (res) => {
        this.productos = res as Productos[];
      },
      (err) => {
        console.log("Error");

      }
    );
  }


  remove(e: any) {
    let row: Productos = e.data;
    this.service.deleteProductos(row).subscribe(
      (res) => {
        console.log("Eliminado!");

      },
      (err) => {
        console.log("Error");
      }
    );
  }

  update(e: any) {
    let row: Productos = e.data;
    this.service.updateProductos(row).subscribe(
      (res) => {
        console.log("Actualizado!");

      },
      (err) => {
        console.log("Error");
      }
    );
  }

  save(e: any) {
    let row: Productos = e.data;
    row.id_producto = 0;
    this.service.saveProductos(row).subscribe(
      (res) => {
        console.log("Guardado!");

      },
      (err) => {
        console.log("Error");
      }
    );
  }



}
